var config = require('./config.json')
var TelegramBot = require('node-telegram-bot-api');
var ytSearch = require('youtube-search');
var youtubedl = require('youtube-dl');
var MPlayer = require('mplayer');
var excludes = ['list','ls','next'];
var queue, bot;

function ini(){
	queue = [];
	bot = new TelegramBot(config.tg, { polling: true });
	setRoute(bot);
}

function listQueue(){
	return Promise.resolve(queue);
}

function removeFromQueue(name){
	var matched  = false;
	queue = queue.filter((queueItem)=>{
		if (queueItem.name == name) matched = true;
		return queueItem.name !== name;
	})
	return Promise.resolve(matched);
}

function getYoutubeId(name){
	return new Promise((resolve)=>{
		ytSearch(name, config.yt, (err, results)=>{
			if (!results || results.length<=0){
				return resolve(null)
			}else{
				var youtubeResult = results[0];
				return resolve({
					name:name,
					title:youtubeResult.title,
					id:youtubeResult.id
				})
			}
		})
	})
}

var Myplayer = {
	player:null,
	play:function(file, onEnd){
		this.player = new MPlayer();
		this.player.openFile(file);
		this.player.on('stop',()=>{
			onEnd();
		})
		this.player.play()
	},
	stop:function(){
		if (this.player) this.player.stop();
	},
	clear:function(){
		queue = [];
		if (this.player) this.player.stop();
	}
}

function addToQueue(item){
	queue.push(item);
	if (queue.length == 1) play();
	return Promise.resolve(true);
}

function play(){
	if (!queue || queue.length == 0) return;
	var queueItem = queue[0];
	var youtubeId =queueItem.id;
	youtubedl.exec(youtubeId, ['--extract-audio', '--audio-format', 'mp3', '-o', 'tmp.%(ext)s'],{},(err,out)=>{
		if (err) return;
		Myplayer.play('tmp.mp3',()=>{
			queue.shift()
			if (queue.length >= 1){
				play();
			}
		});
	})
}

function setRoute(bot){
	bot.onText(/^\/(list|ls)$/i,(msg)=>{
		listQueue().then((playList)=>{
			var playListStr = '你仲未點歌~'
			if (playList && playList.length > 0){
				var cpPlayList = JSON.parse(JSON.stringify(playList));
				playListStr = cpPlayList.reduce((a,b)=>{
					return  (a ? a+"\n" : '>') + '* ' + b.name + " - [" + b.title + "]";
				},'')
			}
			bot.sendMessage(msg.chat.id, playListStr);
		})
	})
	bot.onText(/^\/(remove|rm) (.*)$/i, (msg, matches)=>{
		var name = matches[2].trim();
		removeFromQueue(name).then((status)=>{
			var statusText = status ? '移除左'+name+'啦' : '你無點呢首喎'
			bot.sendMessage(msg.chat.id, statusText)
		})
	})
	bot.onText(/^\/(next|stop)$/i,(msg,matches)=>{
		Myplayer.stop();
		bot.sendMessage(msg.chat.id, '下一首')
	})
	bot.onText(/^\/(cls|clear)$/i,(msg,matches)=>{
		Myplayer.clear();
		bot.sendMessage(msg.chat.id, '清晒')
	})
	bot.on('message',(msg)=>{
		var chatId = msg.chat.id
		var songName = msg.text.trim();
		if (songName.indexOf('/')===0) return;

		getYoutubeId(songName).then((queueItem)=>{
			if (queueItem){
				addToQueue(queueItem).then(()=>{
					bot.sendMessage(chatId, '加左'+queueItem.title+'啦');
				})
			}else{
				bot.sendMessage(chatId, '揾唔到呢首歌喎');
			}
		})
	})
}

ini();
